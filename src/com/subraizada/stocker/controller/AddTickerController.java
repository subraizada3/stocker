/*
 *    Copyright 2018 Subramaniyam Raizada
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.subraizada.stocker.controller;

import com.subraizada.stocker.StockerApp;
import com.subraizada.stocker.util.net.APICommon;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class AddTickerController {
	@FXML
	private VBox addTickerView, progressIndicatorView, retryLoadingView;
	@FXML
	private TextField tickerTextField;
	@FXML
	private Label addTickerFeedbackLabel;

	@FXML
	private void initialize() {
		tryLoadingAllTickers();
		tickerTextField.setTextFormatter(new TextFormatter<>(
				(change -> {
					change.setText(change.getText().toUpperCase());
					return change;
				})
		));
		addTickerFeedbackLabel.setVisible(false);
	}

	private void tryLoadingAllTickers() {
		addTickerView.setVisible(false);
		progressIndicatorView.setVisible(true);
		retryLoadingView.setVisible(false);

		//APICommon.loadAllTickers();
		Task<Void> task = new Task<>() {
			@Override
			public Void call() {
				APICommon.loadAllTickers();
				return null;
			}
		};
		task.setOnSucceeded(event -> actOnTickerLoadStatus());
		new Thread(task).start();
	}

	private void actOnTickerLoadStatus() {
		if (APICommon.allTickersAvailable()) {
			addTickerView.setVisible(true);
			progressIndicatorView.setVisible(false);
			retryLoadingView.setVisible(false);
		} else {
			addTickerView.setVisible(false);
			progressIndicatorView.setVisible(false);
			retryLoadingView.setVisible(true);
		}
	}

	@FXML
	private void addTicker() {
		// if entered ticker is valid, add it to a new tab
		boolean validTicker = APICommon.isValidTicker(tickerTextField.getText());

		if (!validTicker) {
			addTickerFeedbackLabel.setVisible(true);
			return;
		} else {
			// clear any previous error messages
			addTickerFeedbackLabel.setVisible(false);
		}

		// if ticker is already loaded, just switch to that tab
		TabPane tabPane = (TabPane) tickerTextField.getScene().lookup("#tabPane");
		ObservableList<Tab> tabs = tabPane.getTabs();
		Tab switchTab = null;
		for (Tab t : tabs) {
			if (t.getId().equals(tickerTextField.getText())) {
				switchTab = t;
			}
		}
		if (switchTab != null) {
			tabPane.getSelectionModel().select(switchTab);
			// clear the ticker input field before switching
			tickerTextField.setText("");
			return;
		}

		// add ticker as a tab
		try {
			FXMLLoader loader = new FXMLLoader(StockerApp.class.getResource("view/TickerView.fxml"));
			Node rootNode = loader.load();

			Tab tab = new Tab();
			tab.setText(tickerTextField.getText());
			tab.setId(tickerTextField.getText());
			tab.setContent(rootNode);
			tabPane.getTabs().add(tab);
			tabPane.getSelectionModel().select(tab);

			// The nodes in TickerView don't have a parent or scene until after tab.setContent() and
			// tabPane.add(tab) are called, so they can't get a reference to the window's tabPane until now.
			((TickerController) loader.getController()).lateInitialize();

			// clear the ticker input field
			tickerTextField.setText("");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void retryLoadingButtonClicked() {
		tryLoadingAllTickers();
	}
}
