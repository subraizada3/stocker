/*
 *    Copyright 2018 Subramaniyam Raizada
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.subraizada.stocker.controller;

import com.subraizada.stocker.StockerApp;
import com.subraizada.stocker.util.net.APIAlphaVantage;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import java.util.prefs.Preferences;

public class AlphaVantageSetupController {
	@FXML
	WebView webView;
	@FXML
	TextField keyField;
	@FXML
	Label feedbackLabel;

	@FXML
	private void initialize() {
		feedbackLabel.setVisible(false);
		WebEngine engine = webView.getEngine();
		engine.load("https://www.alphavantage.co/support/#api-key");
	}

	// user pressed 'Done' button
	@FXML
	private void doneClicked() {
		// If valid, store entered key and close tab
		if (APIAlphaVantage.isAPIKeyValid(keyField.getText())) {
			// Store API Key
			Preferences prefs = Preferences.userNodeForPackage(StockerApp.class);
			prefs.put("AlphaVantage API Key", keyField.getText());

			// Close this tab
			TabPane tabPane = (TabPane) webView.getScene().lookup("#tabPane");
			tabPane.getTabs().remove(tabPane.getSelectionModel().getSelectedItem());
		}
		// If invalid, say that input is incorrect
		else {
			feedbackLabel.setVisible(true);
		}
	}
}
