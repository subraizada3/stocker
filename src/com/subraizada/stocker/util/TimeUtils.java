/*
 *    Copyright 2018 Subramaniyam Raizada
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.subraizada.stocker.util;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class TimeUtils {

	public static boolean isMarketClosed(ZonedDateTime zdt) {
		if (zdt.getYear() == 2018) {
			int day = zdt.getDayOfMonth();
			switch (zdt.getMonth()) {
				case JANUARY:
					return day == 1 || day == 15;
				case FEBRUARY:
					return day == 19;
				case MARCH:
					return day == 30;
				case MAY:
					return day == 28;
				case JULY:
					return day == 4;
				case SEPTEMBER:
					return day == 3;
				case NOVEMBER:
					return day == 22;
				case DECEMBER:
					return day == 25;
				default:
					return false;
			}
		} else if (zdt.getYear() == 2019) {
			int day = zdt.getDayOfMonth();
			switch (zdt.getMonth()) {
				case JANUARY:
					return day == 1 || day == 21;
				case FEBRUARY:
					return day == 18;
				case APRIL:
					return day == 19;
				case MAY:
					return day == 27;
				case JULY:
					return day == 4;
				case SEPTEMBER:
					return day == 2;
				case NOVEMBER:
					return day == 28;
				case DECEMBER:
					return day == 25;
				default:
					return false;
			}
		} else {
			System.err.println("Market closing database must be refreshed!");
			return false;
		}
	}

	public static boolean isMarketClosedToday() {
		return isMarketClosed(ZonedDateTime.now(ZoneId.of("America/New_York")));
	}

	public static boolean isMarketHoursReduced(ZonedDateTime zdt) {
		if (isMarketClosed(zdt)) return false;

		if (zdt.getYear() == 2018) {
			int day = zdt.getDayOfMonth();
			switch (zdt.getMonth()) {
				case JULY:
					return day == 3;
				case NOVEMBER:
					return day == 23;
				case DECEMBER:
					return day == 24;
				default:
					return false;
			}
		} else if (zdt.getYear() == 2019) {
			int day = zdt.getDayOfMonth();
			switch (zdt.getMonth()) {
				case JULY:
					return day == 3;
				case NOVEMBER:
					return day == 29;
				case DECEMBER:
					return day == 24;
				default:
					return false;
			}
		} else {
			System.err.println("Market closing database must be refreshed!");
			return false;
		}

	}

	public static boolean isMarketHoursReducedToday() {
		return isMarketHoursReduced(ZonedDateTime.now(ZoneId.of("America/New_York")));
	}

	public static boolean isMarketOpen(ZonedDateTime zdt) {
		// if market is closed today, return false
		if (isMarketClosedToday()) return false;
		// if it is before 9:30 AM, return false
		if (zdt.getHour() <= 8 || (zdt.getHour() == 9 && zdt.getMinute() < 30)) return false;
		// if it is past closing time, return false
		// closingTime = 16:00 or 13:00
		int closingTime = isMarketHoursReducedToday() ? 13 : 16;
		return zdt.getHour() < closingTime;
	}

	public static boolean isMarketOpenNow() {
		return isMarketOpen(ZonedDateTime.now(ZoneId.of("America/New_York")));
	}

	// in seconds
	public static int getTimeToNextUpdate(TimeFrame timeFrame) {
		ZonedDateTime zdt = ZonedDateTime.now(ZoneId.of("America/New_York"));
		switch (timeFrame) {
			case PERIOD_5m:
			case TOTAL_5d:
				// refresh 1 second after the next 5-minute block switches
				return timeToNextMinuteBlock(zdt.getMinute(), 5);
			case PERIOD_15m:
				// refresh 1 second after the next 15-minute block switches
				return timeToNextMinuteBlock(zdt.getMinute(), 15);
			case PERIOD_30m:
				// refresh 1 second after the next 30-minute block switches
				return timeToNextMinuteBlock(zdt.getMinute(), 30);
			case PERIOD_1h:
				// refresh 1 second after the next 60-minute block switches
				return timeToNextMinuteBlock(zdt.getMinute(), 60);
			case PERIOD_1d:
			case TOTAL_1M:
			case TOTAL_3M:
			case TOTAL_6M:
			case TOTAL_1y:
			case TOTAL_2y:
				// 1w/5y cases don't really belong here, but easier to update daily than weekly
			case PERIOD_1w:
			case TOTAL_5y: {
				// If market currently open, update 1 second after close.
				// However, IEX api can take some time to get closing info,
				// so if within 45 minutes of close, refresh every 5 minutes.
				// Otherwise, return next day's close.
				int closingHour = isMarketHoursReducedToday() ? 13 : 16;
				if (isMarketOpenNow() || zdt.getHour() < closingHour) {
					// algorithm is same for in market hours and in the morning before market opens
					int hoursUntilClose = closingHour - zdt.getHour() - 1; // subtract 1
					int minutesUntilClose = 60 - zdt.getMinute();
					return hoursUntilClose * 60 * 60 + minutesUntilClose * 60 + 1;
				} else if (zdt.getHour() == closingHour) {
					// just refreshing every 5 minutes for an hours after close is
					// easier than checking for 45 minutes
					return timeToNextMinuteBlock(zdt.getMinute(), 5);
				} else {
					// night after close - return time to next day's close
					ZonedDateTime tomorrow = zdt.plusDays(1);
					closingHour = isMarketHoursReduced(tomorrow) ? 13 : 16;
					closingHour += 24;
					int hoursUntilClose = closingHour = zdt.getHour();
					int minutesUntilClose = 60 - zdt.getMinute();
					return hoursUntilClose * 60 * 60 + minutesUntilClose * 60 + 1;
				}
			}
			case PERIOD_1m:
			case TOTAL_1d:
			default:
				// refresh 1 second after the minute switches
				return 61 - zdt.getSecond();
		}
	}

	private static int timeToNextMinuteBlock(int currentMinute, int blockSize) {
		int overflowMinutes = currentMinute % blockSize;
		int minutesLeft = blockSize - overflowMinutes;
		return minutesLeft * 60 + 1;
	}
}
