/*
 *    Copyright 2018 Subramaniyam Raizada
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.subraizada.stocker.model;

import java.time.LocalDateTime;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;

public class QuoteHistorical implements Comparable<QuoteHistorical> {
	private StringProperty timeStr;
	private LocalDateTime time;
	private FloatProperty open, low, high, close;
	private IntegerProperty volume;

	public QuoteHistorical(StringProperty timeStr, LocalDateTime time, FloatProperty open, FloatProperty low, FloatProperty high, FloatProperty close, IntegerProperty volume) {
		this.timeStr = timeStr;
		this.time = time;
		this.open = open;
		this.low = low;
		this.high = high;
		this.close = close;
		this.volume = volume;
	}

	@Override
	public int compareTo(QuoteHistorical o) {
		return getTime().compareTo(o.getTime()) * -1; // negate to have recent dates first
	}

	public String getTimeStr() {
		return timeStr.get();
	}

	public void setTimeStr(String timeStr) {
		this.timeStr.set(timeStr);
	}

	public StringProperty timeStrProperty() {
		return timeStr;
	}

	public LocalDateTime getTime() {
		return time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}

	public float getOpen() {
		return open.get();
	}

	public void setOpen(float open) {
		this.open.set(open);
	}

	public FloatProperty openProperty() {
		return open;
	}

	public float getLow() {
		return low.get();
	}

	public void setLow(float low) {
		this.low.set(low);
	}

	public FloatProperty lowProperty() {
		return low;
	}

	public float getHigh() {
		return high.get();
	}

	public void setHigh(float high) {
		this.high.set(high);
	}

	public FloatProperty highProperty() {
		return high;
	}

	public float getClose() {
		return close.get();
	}

	public void setClose(float close) {
		this.close.set(close);
	}

	public FloatProperty closeProperty() {
		return close;
	}

	public int getVolume() {
		return volume.get();
	}

	public void setVolume(int volume) {
		this.volume.set(volume);
	}

	public IntegerProperty volumeProperty() {
		return volume;
	}
}
