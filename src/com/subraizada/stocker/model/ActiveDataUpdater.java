/*
 *    Copyright 2018 Subramaniyam Raizada
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.subraizada.stocker.model;

import com.subraizada.stocker.util.net.APICommon;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ActiveDataUpdater {
	private static ScheduledExecutorService service;
	private static ScheduledExecutorService outOfDateSetter;

	// start auto refresh
	public static void start() {
		service = Executors.newScheduledThreadPool(1, r -> {
			Thread t = new Thread(r);
			t.setDaemon(true);
			return t;
		});
		service.scheduleAtFixedRate(ActiveDataUpdater::update, 0, 250, TimeUnit.MILLISECONDS);
		outOfDateSetter = Executors.newScheduledThreadPool(1);
	}

	// stop auto refresh
	public static void stop() {
		service.shutdownNow();
		outOfDateSetter.shutdownNow();
	}

	// request a refresh right now
	public static void refresh() {
		service.submit(ActiveDataUpdater::update);
	}

	private static void update() {
		// return if no valid ticker selected
		if (ActiveData.getCurrentTicker().equals("")) {
			ActiveData.reset();
			return;
		}

		try {
			ActiveData.getQuote().update(APICommon.getQuote(ActiveData.getCurrentTicker()));
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

		// since update was successful, add thread to mark data as out of date after two seconds
		ActiveData.setOutOfDate(false);
		outOfDateSetter.shutdownNow(); // stop any old threads that will mark it as out of date
		outOfDateSetter = Executors.newScheduledThreadPool(1);
		outOfDateSetter.schedule(() -> ActiveData.setOutOfDate(true), 2, TimeUnit.SECONDS);
	}
}
