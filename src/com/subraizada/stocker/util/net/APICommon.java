/*
 *    Copyright 2018 Subramaniyam Raizada
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.subraizada.stocker.util.net;

import com.subraizada.stocker.model.QuoteHistorical;
import com.subraizada.stocker.model.QuoteRealTime;
import com.subraizada.stocker.util.Debugger;
import com.subraizada.stocker.util.TimeFrame;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

// Provides a common interface to get quotes and data, abstracting the AlphaVantage and IEX APIs
public class APICommon {
	private static String[] allTickers;

	public static void loadAllTickers() {
		// save the list of tickers into a string
		String jsonStr;
		try {
			jsonStr = IOUtils.toString(new URL("https://api.iextrading.com/1.0/ref-data/symbols"));
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

		JSONArray json = new JSONArray(jsonStr);
		allTickers = new String[json.length()];

		for (int i = 0; i < json.length(); i++) {
			JSONObject obj = json.getJSONObject(i);
			// add to all tickers if it has a ticker and it is enabled for trading on IEX
			if (obj.has("symbol") && obj.has("isEnabled") && ((Boolean) obj.get("isEnabled")))
				allTickers[i] = (String) obj.get("symbol");
		}
	}

	public static boolean allTickersAvailable() {
		return allTickers != null && allTickers.length > 0;
	}

	public static boolean isValidTicker(String ticker) {
		return Arrays.asList(allTickers).contains(ticker);
	}

	public static QuoteRealTime getQuote(String ticker) throws IOException {
		return APIIEX.getQuote(ticker);
	}

	public static ArrayList<QuoteHistorical> getChart(String ticker, TimeFrame timeFrame) throws IOException {
		Debugger.log("Common API requesting chart for " + timeFrame);
		switch (timeFrame) {
			case TOTAL_1d:
			case PERIOD_1m:
				return APIAlphaVantage.getIntradayChart(ticker, timeFrame, true);
			case TOTAL_5d:
			case PERIOD_5m:
				return APIAlphaVantage.getIntradayChart(ticker, timeFrame, true);
			case PERIOD_15m:
				return APIAlphaVantage.getIntradayChart(ticker, timeFrame, true);
			case PERIOD_30m:
				return APIAlphaVantage.getIntradayChart(ticker, timeFrame, true);
			case PERIOD_1h:
				return APIAlphaVantage.getIntradayChart(ticker, timeFrame, true);
			case TOTAL_1M:
				return APIIEX.getChart(ticker, timeFrame);
			case TOTAL_3M:
				return APIIEX.getChart(ticker, timeFrame);
			case TOTAL_6M:
				return APIIEX.getChart(ticker, timeFrame);
			case TOTAL_1y:
				return APIIEX.getChart(ticker, timeFrame);
			case PERIOD_1w:
			case TOTAL_5y:
				return APIIEX.getChart(ticker, timeFrame);
			case PERIOD_1d:
			case TOTAL_2y:
			default:
				return APIIEX.getChart(ticker, timeFrame);
		}
	}
}
