/*
 *    Copyright 2018 Subramaniyam Raizada
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.subraizada.stocker.model;

import com.subraizada.stocker.util.Debugger;
import com.subraizada.stocker.util.TimeUtils;
import com.subraizada.stocker.util.net.APICommon;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

// TODO: check that this is updating correctly
// Intraday 1/5/15/30/60 minutes
// At end of day
public class ChartDataUpdater {
	private static ScheduledExecutorService service;
	private static ScheduledExecutorService outOfDateSetter;

	// start auto refresh
	public static void start() {
		service = Executors.newScheduledThreadPool(1, r -> {
			Thread t = new Thread(r);
			t.setDaemon(true);
			return t;
		});
		service.schedule(ChartDataUpdater::update, 0, TimeUnit.SECONDS);
		outOfDateSetter = Executors.newScheduledThreadPool(1);
	}

	// stop auto refresh
	public static void stop() {
		service.shutdownNow();
		outOfDateSetter.shutdownNow();
	}

	// request a refresh right now
	public static void refresh() {
		// since chart refreshes can take some time, just submitting a new update request isn't enough
		// instead, stop any current requests and start a new one
		ChartData.setOutOfDate(true);
		stop();
		start();
	}

	private static void update() {
		// return if no valid ticker selected
		if (ChartData.getCurrentTicker().equals("")) {
			ChartData.reset();
			return;
		}

		try {
			ArrayList<QuoteHistorical> newChart = APICommon.getChart(ChartData.getCurrentTicker(), ChartData.getTimeFrame());
			ObservableList<QuoteHistorical> chart = ChartData.getTimeSeries();
			chart.clear();
			chart.addAll(newChart);
			FXCollections.sort(chart);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

		Debugger.log("Chart update done");

		// schedule the next update based on the selected timeframe
		int secondsToNextUpdate = TimeUtils.getTimeToNextUpdate(ChartData.getTimeFrame());
		service.schedule(ChartDataUpdater::update, secondsToNextUpdate, TimeUnit.SECONDS);

		// mark data as out of date 5 seconds after it should have been renewed
		ChartData.setOutOfDate(false);
		outOfDateSetter.shutdownNow(); // stop any old threads that will mark it as out of date
		outOfDateSetter = Executors.newScheduledThreadPool(1);
		outOfDateSetter.schedule(() -> ChartData.setOutOfDate(true), secondsToNextUpdate + 5, TimeUnit.SECONDS);
	}
}
