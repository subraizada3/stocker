/*
 *    Copyright 2018 Subramaniyam Raizada
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.subraizada.stocker.controller;

import com.subraizada.stocker.model.ActiveData;
import com.subraizada.stocker.util.Formatting;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

public class TickerController {
	@FXML
	private Label tickerID, price, bid, ask, spread, change, changeDirection, iexAttributionFlagSpacer;
	@FXML
	private Label open, close, high, low;
	@FXML
	private Node rootNode, marketOpenFlag, marketClosedFlag, delayedPriceFlag, outOfDateFlag, iexAttributionFlag;
	// only using setVisible() method on the above variables, so declare them as Nodes to reduce
	// dependence between code and FXML layout
	@FXML
	private Pane chartViewPane;

	@FXML
	private void initialize() {
		price.setText("");
		bid.setText("");
		ask.setText("");
		spread.setText("");
		change.setText("");
		changeDirection.setText(""); // \U2022 = •, \U2192 = ↑, \U2193 = ↓

		outOfDateFlag.setVisible(true);
		open.setText("");
		close.setText("");
		high.setText("");
		low.setText("");

		marketOpenFlag.setVisible(true);
		marketClosedFlag.setVisible(false);
		delayedPriceFlag.setVisible(false);
		iexAttributionFlagSpacer.setVisible(false);
		iexAttributionFlag.setVisible(false);

		// add listeners to update values on screen
		ActiveData.getQuote().priceProperty().addListener(
				(observable, oldValue, newValue) -> Platform.runLater(this::setPrice));
		ActiveData.getQuote().bidProperty().addListener(
				(observable, oldValue, newValue) -> Platform.runLater(this::setBidAskSpread));
		ActiveData.getQuote().askProperty().addListener(
				(observable, oldValue, newValue) -> Platform.runLater(this::setBidAskSpread));
		ActiveData.getQuote().changePercentProperty().addListener(
				(observable, oldValue, newValue) -> Platform.runLater(this::setChangeText));
		ActiveData.getQuote().changeDollarsProperty().addListener(
				(observable, oldValue, newValue) -> Platform.runLater(this::setChangeText));
		ActiveData.getQuote().afterHoursProperty().addListener(
				((observable, oldValue, newValue) -> Platform.runLater(this::setFlagsVisibility))
		);
		ActiveData.getQuote().delayedPriceProperty().addListener(
				((observable, oldValue, newValue) -> Platform.runLater(this::setFlagsVisibility))
		);
		ActiveData.outOfDateProperty().addListener((observable, oldValue, newValue) ->
				Platform.runLater(() -> outOfDateFlag.setVisible(newValue)));
		ActiveData.getQuote().openProperty().addListener((observable, oldValue, newValue) ->
				Platform.runLater(() ->
						open.setText(Formatting.toDecimalPlacesMinMax(newValue.floatValue(), 2, 3))));
		ActiveData.getQuote().closeProperty().addListener((observable, oldValue, newValue) ->
				Platform.runLater(() ->
						close.setText(Formatting.toDecimalPlacesMinMax(newValue.floatValue(), 2, 3))));
		ActiveData.getQuote().highProperty().addListener((observable, oldValue, newValue) ->
				Platform.runLater(() ->
						high.setText(Formatting.toDecimalPlacesMinMax(newValue.floatValue(), 2, 3))));
		ActiveData.getQuote().lowProperty().addListener((observable, oldValue, newValue) ->
				Platform.runLater(() ->
						low.setText(Formatting.toDecimalPlacesMinMax(newValue.floatValue(), 2, 3))));
	}

	private void setFlagsVisibility() {
		// market open, real time price
		if (!ActiveData.getQuote().isAfterHours() && !ActiveData.getQuote().isDelayedPrice()) {
			marketOpenFlag.setVisible(true);
			marketClosedFlag.setVisible(false);
			delayedPriceFlag.setVisible(false);
			// should be displayed when using TOPS data
			iexAttributionFlag.setVisible(true);
		}
		// delayed price
		else if (!ActiveData.getQuote().isAfterHours() && ActiveData.getQuote().isDelayedPrice()) {
			marketOpenFlag.setVisible(false);
			marketClosedFlag.setVisible(false);
			delayedPriceFlag.setVisible(true);
			iexAttributionFlag.setVisible(false);
		}
		// market closed
		else if (ActiveData.getQuote().isAfterHours()) {
			marketOpenFlag.setVisible(false);
			marketClosedFlag.setVisible(true);
			delayedPriceFlag.setVisible(false);
			iexAttributionFlag.setVisible(false);
		}
	}

	void lateInitialize() {
		TabPane tabPane = (TabPane) rootNode.getParent().getParent();
		Tab tab = tabPane.getSelectionModel().getSelectedItem();
		String ticker = tab.getId();

		tickerID.setText(ticker);

		// get width of ticker text and set iex attribution to correct size
		tickerID.applyCss();
		iexAttributionFlagSpacer.setPrefWidth(tickerID.prefWidth(-1) + ((HBox) tickerID.getParent()).getSpacing() + 4);
	}

	private void setChangeText() {
		String changeString = "";
		String changeDollarsString = Formatting.toDecimalPlacesMin(ActiveData.getQuote().getChangeDollars(), 2),
				changePercentString = Formatting.toDecimalPlacesMinMax(ActiveData.getQuote().getChangePercent() * 100, 2, 3);

		// converting number to string automatically adds - sign, but does not add + sign, so add + if it is not 0
		// if string is negative, don't change
		// if string is not negative and the change is less than 0.5 cents, don't change
		// if the string is not negative and the change is >= than 0.5 cents, add + sign to beginning
		if (!changeDollarsString.substring(0, 1).equals("-") && Math.abs(ActiveData.getQuote().getChangeDollars()) > 0.004) {
			changeDollarsString = "+" + changeDollarsString;
			changePercentString = "+" + changePercentString;
		}

		changeString += changeDollarsString + " (" + changePercentString + "%)";
		change.setText(changeString);

		if (ActiveData.getQuote().getChangeDollars() > 0) {
			changeDirection.setText("↑");
			change.setStyle("-fx-text-fill: green");
			changeDirection.setStyle("-fx-text-fill: green");
		} else if (ActiveData.getQuote().getChangeDollars() < 0) {
			changeDirection.setText("↓");
			change.setStyle("-fx-text-fill: red");
			changeDirection.setStyle("-fx-text-fill: red");
		} else {
			changeDirection.setText("•");
			change.setStyle("-fx-text-fill: inherit");
			change.setStyle("-fx-text-fill: inherit");
		}
	}

	private void setPrice() {
		String priceStr = Formatting.toDecimalPlacesMin(ActiveData.getQuote().getPrice(), 2);
		price.setText(priceStr);
	}

	private void setBidAskSpread() {
		bid.setText(Formatting.toDecimalPlacesMin(ActiveData.getQuote().getBid(), 2));
		ask.setText(Formatting.toDecimalPlacesMin(ActiveData.getQuote().getAsk(), 2));
		spread.setText(Formatting.toDecimalPlacesMinMax(ActiveData.getQuote().getAsk() - ActiveData.getQuote().getBid(), 2, 3));
	}
}
