# Stocker

A stock price & chart viewer, based on JavaFX. Currently in development

Data acquired using the [IEX](https://iextrading.com/developer/) and [AlphaVantage](https://www.alphavantage.co/) APIs.


The chart view is still in progress; here is a screenshot of the current state of the project (as of 01/18/2018):

![stocker screenshot](https://bytebucket.org/subraizada3/stocker/raw/fe85fd941a25c7b741a438dcf92b5e1941b85672/screenshot.png)

## Current Status
Spring 2018 has been a relatively busy semester for me, and the use case for which I started making this is no longer necessary - I do not have the time to actively trade stocks.
I definitely will complete this application, but it is at a slightly lower priority in my life.

Once I'm done with the [crypto trader](https://bitbucket.org/subraizada3/limit-trader-for-gdax), I will add in an API here to allow for cryptocurrencies to also be viewed, and then will continue work on the chart view.
