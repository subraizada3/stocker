/*
 *    Copyright 2018 Subramaniyam Raizada
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.subraizada.stocker.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

public class ActiveData {
	private static String currentTicker = "";
	private static BooleanProperty outOfDate = new SimpleBooleanProperty(true);
	private static QuoteRealTime quote = new QuoteRealTime();

	public static void reset() {
		setCurrentTicker("");
		setOutOfDate(true);
		quote.reset();
	}

	public static String getCurrentTicker() {
		return currentTicker;
	}

	public static void setCurrentTicker(String currentTicker) {
		ActiveData.currentTicker = currentTicker;
	}

	public static boolean isOutOfDate() {
		return outOfDate.get();
	}

	public static void setOutOfDate(boolean outOfDate) {
		ActiveData.outOfDate.set(outOfDate);
	}

	public static BooleanProperty outOfDateProperty() {
		return outOfDate;
	}

	public static QuoteRealTime getQuote() {
		return quote;
	}

	public static void setQuote(QuoteRealTime quote) {
		ActiveData.quote = quote;
	}

}
