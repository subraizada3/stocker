/*
 *    Copyright 2018 Subramaniyam Raizada
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.subraizada.stocker.controller;

import com.subraizada.stocker.candleStickChartType2.AdvCandleStickChart;
import com.subraizada.stocker.model.ChartData;
import com.subraizada.stocker.model.ChartDataUpdater;
import com.subraizada.stocker.util.TimeFrame;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class ChartController {
	@FXML
	private RadioButton chart_time_period_1m, chart_time_period_5m, chart_time_period_15m, chart_time_period_30m, chart_time_period_1h, chart_time_period_1d, chart_time_period_1w,
			chart_time_total_1d, chart_time_total_5d, chart_time_total_3m, chart_time_total_6m, chart_time_total_1y, chart_time_total_2y, chart_time_total_5y,
			chart_display_chartType_candle, chart_display_chartType_line;
	@FXML
	private ToggleButton chart_display_candleFill_up, chart_display_candleFill_down;
	@FXML
	private ToggleGroup chart_display_chartType_group, chart_time_group;
	@FXML
	private Label chartOutOfDateFlag;

	@FXML
	private VBox quoteChartLoadingIndicator;
	@FXML
	private Pane chartPane;

	@FXML
	private void initialize() {
		// add chart to chartPane
		chartPane.getChildren().add(new AdvCandleStickChart());

		// add chart out of date display
		ChartData.outOfDateProperty().addListener((observable, oldValue, newValue) -> {
					chartOutOfDateFlag.setVisible(newValue);
					// if not out of date, hide loading / show chart
					if (!newValue) {
						quoteChartLoadingIndicator.setVisible(false);
						chartPane.setVisible(true);
					}
				}
		);

		// add chart candle/line toggles
		chart_display_chartType_group.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
			// TODO: update chart renderer with candle/line selection
		});

		// add chart timeframe buttons
		chart_time_group.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
			RadioButton newButton = (RadioButton) newValue;
			String prefixString = "";
			if (newButton.getParent().getId().equals("chart_time_period_group")) {
				prefixString = "period ";
			} else if (newButton.getParent().getId().equals("chart_time_total_group")) {
				prefixString = "total ";
			}
			switch (newButton.getId()) {
				case "chart_time_period_1m":
					ChartData.setTimeFrame(TimeFrame.PERIOD_1m);
					break;
				case "chart_time_period_5m":
					ChartData.setTimeFrame(TimeFrame.PERIOD_5m);
					break;
				case "chart_time_period_15m":
					ChartData.setTimeFrame(TimeFrame.PERIOD_15m);
					break;
				case "chart_time_period_30m":
					ChartData.setTimeFrame(TimeFrame.PERIOD_30m);
					break;
				case "chart_time_period_1h":
					ChartData.setTimeFrame(TimeFrame.PERIOD_1h);
					break;
				case "chart_time_period_1d":
					ChartData.setTimeFrame(TimeFrame.PERIOD_1d);
					break;
				case "chart_time_period_1w":
					ChartData.setTimeFrame(TimeFrame.PERIOD_1w);
					break;
				case "chart_time_total_1d":
					ChartData.setTimeFrame(TimeFrame.TOTAL_1d);
					break;
				case "chart_time_total_5d":
					ChartData.setTimeFrame(TimeFrame.TOTAL_5d);
					break;
				case "chart_time_total_3m":
					ChartData.setTimeFrame(TimeFrame.TOTAL_3M);
					break;
				case "chart_time_total_6m":
					ChartData.setTimeFrame(TimeFrame.TOTAL_6M);
					break;
				case "chart_time_total_1y":
					ChartData.setTimeFrame(TimeFrame.TOTAL_1y);
					break;
				case "chart_time_total_2y":
					ChartData.setTimeFrame(TimeFrame.TOTAL_2y);
					break;
				case "chart_time_total_5y":
					ChartData.setTimeFrame(TimeFrame.TOTAL_5y);
					break;
			}
			// refresh, hide chart / show loading
			ChartDataUpdater.refresh();
			quoteChartLoadingIndicator.setVisible(true);
			chartPane.setVisible(false);
		});
	}

	@FXML
	private void chart_display_candleFillButtonClicked() {
		// TODO: update chart render with fill on up/down
	}
}
