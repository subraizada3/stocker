/*
 *    Copyright 2018 Subramaniyam Raizada
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.subraizada.stocker.controller;

import com.subraizada.stocker.StockerApp;
import com.subraizada.stocker.model.ActiveData;
import com.subraizada.stocker.model.ActiveDataUpdater;
import com.subraizada.stocker.model.ChartData;
import com.subraizada.stocker.model.ChartDataUpdater;
import com.subraizada.stocker.model.CurrentDataUpdater;
import com.subraizada.stocker.util.net.APICommon;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;

public class RootViewController {
	@FXML
	private TabPane tabPane;
	@FXML
	private Tab addTickerTab;

	@FXML
	private void initialize() {
		// add initial '+' tab
		try {
			addTickerTab.setId("+");
			addTickerTab.setContent(FXMLLoader.load(StockerApp.class.getResource("view/AddTickerView.fxml")));
		} catch (IOException e) {
			e.printStackTrace();
		}

		// add listener to update selected ticker on TabPane selection change
		tabPane.getSelectionModel().selectedItemProperty().addListener(
				(observable, oldValue, newValue) -> {
					String tabID = newValue.getId();

					// clear data
					ActiveData.reset();
					ChartData.reset();

					// update selected tab
					if (tabID == null || !APICommon.isValidTicker(tabID)) {
						ActiveData.setCurrentTicker("");
						ChartData.setCurrentTicker("");
					} else {
						ActiveData.setCurrentTicker(tabID);
						ChartData.setCurrentTicker(tabID);
					}

					// refresh data
					ActiveDataUpdater.refresh();
					ChartDataUpdater.refresh();
					CurrentDataUpdater.refresh();
				}
		);

		// start data updaters
		ActiveDataUpdater.start();
		ChartDataUpdater.start();
		CurrentDataUpdater.start(tabPane);

		// add listeners for ctrl-t and ctrl-w
		KeyCombination ctrlW = new KeyCodeCombination(KeyCode.W, KeyCombination.CONTROL_DOWN);
		KeyCombination ctrlT = new KeyCodeCombination(KeyCode.T, KeyCombination.CONTROL_DOWN);
		tabPane.addEventHandler(KeyEvent.KEY_RELEASED, event -> {
			// ctrl-w
			if (ctrlW.match(event)) {
				Tab currentTab = tabPane.getSelectionModel().getSelectedItem();
				if (currentTab.isClosable())
					tabPane.getTabs().remove(currentTab);
			}
			// ctrl-t
			else if (ctrlT.match(event)) {
				tabPane.getSelectionModel().select(0);
				// give focus to the new ticker input box
				tabPane.getScene().lookup("#tickerTextField").requestFocus();
			}
		});

		// add listener to switch to new tab page on ctrl-t
		tabPane.addEventFilter(KeyEvent.KEY_RELEASED, event -> {
		});
	}
}

