/*
 *    Copyright 2018 Subramaniyam Raizada
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

// design inspired by a version of the Ensemble candle stick chart
// https://gist.github.com/Nicolas56/e02b29431b820d7c8c218c804f0269b0

package com.subraizada.stocker.controller;

import javafx.scene.chart.Axis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.Pane;

public class Chart extends Pane {
	private NumberAxis xAxis, yAxis;
	private XYChart<Double, Double> chart;

	public Chart() {
		xAxis = new NumberAxis(0, 1, 1);
		xAxis.setAutoRanging(true);
		xAxis.setForceZeroInRange(false);
		xAxis.setAnimated(false);

		yAxis = new NumberAxis(0, 1, 1);
		yAxis.setAutoRanging(true);
		yAxis.setForceZeroInRange(false);
		yAxis.setAnimated(false);

		chart = new CandleStickChart((Axis) xAxis, (Axis) yAxis);
	}

	private class CandleStickChart extends XYChart<Double, Double> {

		public CandleStickChart(Axis<Double> doubleAxis, Axis<Double> doubleAxis2) {
			super(doubleAxis, doubleAxis2);
		}

		@Override
		protected void dataItemAdded(Series<Double, Double> series, int itemIndex, Data<Double, Double> item) {

		}

		@Override
		protected void dataItemRemoved(Data<Double, Double> item, Series<Double, Double> series) {

		}

		@Override
		protected void dataItemChanged(Data<Double, Double> item) {

		}

		@Override
		protected void seriesAdded(Series<Double, Double> series, int seriesIndex) {

		}

		@Override
		protected void seriesRemoved(Series<Double, Double> series) {

		}

		@Override
		protected void layoutPlotChildren() {

		}
	}
}
