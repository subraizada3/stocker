/*
 *    Copyright 2018 Subramaniyam Raizada
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.subraizada.stocker;

import com.subraizada.stocker.model.ActiveDataUpdater;
import com.subraizada.stocker.model.ChartDataUpdater;
import com.subraizada.stocker.model.CurrentDataUpdater;
import com.subraizada.stocker.util.Debugger;
import com.subraizada.stocker.util.net.APIAlphaVantage;
import java.io.IOException;
import java.util.prefs.Preferences;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class StockerApp extends Application {

	// https://docs.oracle.com/javase/8/javase-clienttechnologies.htm

	private Stage primaryStage;
	private BorderPane rootPane;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		Debugger.enabled = true;

		this.primaryStage = primaryStage;

		rootPane = FXMLLoader.load(getClass().getResource("view/RootView.fxml"));
		primaryStage.setTitle("Stocker");
		primaryStage.getIcons().add(new Image("file:res/icon-32.png"));
		primaryStage.getIcons().add(new Image("file:res/icon-64.png"));
		primaryStage.setScene(new Scene(rootPane, 1300, 800));
		primaryStage.show();

		alphaVantageAPISetup();
	}

	private void alphaVantageAPISetup() {
		Preferences prefs = Preferences.userNodeForPackage(StockerApp.class);
		String currentKey = prefs.get("AlphaVantage API Key", "");

		if (!APIAlphaVantage.isAPIKeyValid(currentKey)) {
			try {
				// make tab with AV API Key setup and add to TabPane
				TabPane tabPane = (TabPane) rootPane.getScene().lookup("#tabPane");
				Tab tab = new Tab();
				tab.setText("Initial Setup");
				tab.setClosable(true);
				tabPane.getTabs().add(tab);
				tab.setContent(FXMLLoader.load(getClass().getResource("view/AlphaVantageSetupView.fxml")));
				tabPane.getSelectionModel().select(tab);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// API key has been set, load it into the AlphaVantage class
		currentKey = prefs.get("AlphaVantage API Key", "");
		APIAlphaVantage.setAPIKey(currentKey);
	}

	@Override
	public void stop() throws Exception {
		ActiveDataUpdater.stop();
		ChartDataUpdater.stop();
		CurrentDataUpdater.stop();
		super.stop();
	}
}
