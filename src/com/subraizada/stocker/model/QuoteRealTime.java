/*
 *    Copyright 2018 Subramaniyam Raizada
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.subraizada.stocker.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleFloatProperty;

public class QuoteRealTime {
	private BooleanProperty afterHours = new SimpleBooleanProperty(),
			delayedPrice = new SimpleBooleanProperty();
	private FloatProperty price = new SimpleFloatProperty(),
			bid = new SimpleFloatProperty(),
			ask = new SimpleFloatProperty(),
			changeDollars = new SimpleFloatProperty(),
			changePercent = new SimpleFloatProperty(),
			open = new SimpleFloatProperty(),
			close = new SimpleFloatProperty(),
			high = new SimpleFloatProperty(),
			low = new SimpleFloatProperty();

	public QuoteRealTime() {
		reset();
	}

	public QuoteRealTime(BooleanProperty afterHours, BooleanProperty delayedPrice, FloatProperty price, FloatProperty bid, FloatProperty ask, FloatProperty changeDollars, FloatProperty changePercent, FloatProperty open, FloatProperty close, FloatProperty high, FloatProperty low) {
		this.afterHours = afterHours;
		this.delayedPrice = delayedPrice;
		this.price = price;
		this.bid = bid;
		this.ask = ask;
		this.changeDollars = changeDollars;
		this.changePercent = changePercent;
		this.open = open;
		this.close = close;
		this.high = high;
		this.low = low;
	}

	public void update(QuoteRealTime newQuote) {
		afterHours.set(newQuote.isAfterHours());
		delayedPrice.set(newQuote.isDelayedPrice());
		price.set(newQuote.getPrice());
		bid.set(newQuote.getBid());
		ask.set(newQuote.getAsk());
		changeDollars.set(newQuote.getChangeDollars());
		changePercent.set(newQuote.getChangePercent());
		open.set(newQuote.getOpen());
		close.set(newQuote.getClose());
		high.set(newQuote.getHigh());
		low.set(newQuote.getLow());
	}

	public void reset() {
		setPrice(0);
		setBid(0);
		setAsk(0);
		setChangeDollars(0);
		setChangePercent(0);

		setOpen(0);
		setClose(0);
		setHigh(0);
		setLow(0);

		setAfterHours(false);
		setDelayedPrice(false);
	}

	public boolean isAfterHours() {
		return afterHours.get();
	}

	public void setAfterHours(boolean afterHours) {
		this.afterHours.set(afterHours);
	}

	public BooleanProperty afterHoursProperty() {
		return afterHours;
	}

	public boolean isDelayedPrice() {
		return delayedPrice.get();
	}

	public void setDelayedPrice(boolean delayedPrice) {
		this.delayedPrice.set(delayedPrice);
	}

	public BooleanProperty delayedPriceProperty() {
		return delayedPrice;
	}

	public float getPrice() {
		return price.get();
	}

	public void setPrice(float price) {
		this.price.set(price);
	}

	public FloatProperty priceProperty() {
		return price;
	}

	public float getBid() {
		return bid.get();
	}

	public void setBid(float bid) {
		this.bid.set(bid);
	}

	public FloatProperty bidProperty() {
		return bid;
	}

	public float getAsk() {
		return ask.get();
	}

	public void setAsk(float ask) {
		this.ask.set(ask);
	}

	public FloatProperty askProperty() {
		return ask;
	}

	public float getChangeDollars() {
		return changeDollars.get();
	}

	public void setChangeDollars(float changeDollars) {
		this.changeDollars.set(changeDollars);
	}

	public FloatProperty changeDollarsProperty() {
		return changeDollars;
	}

	public float getChangePercent() {
		return changePercent.get();
	}

	public void setChangePercent(float changePercent) {
		this.changePercent.set(changePercent);
	}

	public FloatProperty changePercentProperty() {
		return changePercent;
	}

	public float getOpen() {
		return open.get();
	}

	public void setOpen(float open) {
		this.open.set(open);
	}

	public FloatProperty openProperty() {
		return open;
	}

	public float getClose() {
		return close.get();
	}

	public void setClose(float close) {
		this.close.set(close);
	}

	public FloatProperty closeProperty() {
		return close;
	}

	public float getHigh() {
		return high.get();
	}

	public void setHigh(float high) {
		this.high.set(high);
	}

	public FloatProperty highProperty() {
		return high;
	}

	public float getLow() {
		return low.get();
	}

	public void setLow(float low) {
		this.low.set(low);
	}

	public FloatProperty lowProperty() {
		return low;
	}
}
