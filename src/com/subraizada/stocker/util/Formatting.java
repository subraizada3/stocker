/*
 *    Copyright 2018 Subramaniyam Raizada
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.subraizada.stocker.util;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Formatting {
	public static String toDecimalPlacesMinMax(float number, int minPlaces, int maxPlaces) {
		String str = String.valueOf(number);
		String[] parts = str.split("\\.");

		// if number is an integer, use String.format("%.xf", number), where x == minPlaces
		if (parts.length == 1) {
			return String.format("%." + minPlaces + "f", number);
		}

		// otherwise, use length of decimal part to format correctly
		String decPart = parts[1];

		// not enough decimal places - same as if number is an integer
		if (decPart.length() < minPlaces) {
			return String.format("%." + minPlaces + "f", number);
		}
		// too many decimal places - use String.format(), but with maxPlaces - will automatically round
		else if (decPart.length() > maxPlaces) {
			// however, with min/max of 2 & 4, 1.01597 becomes 1.0160, so need to remove trailing zeroes
			DecimalFormat format = new DecimalFormat();
			format.setMaximumFractionDigits(maxPlaces);
			format.setRoundingMode(RoundingMode.HALF_UP);
			//return String.format("%." + maxPlaces + "f", number);
			return format.format(number);
		}
		// if there are enough and not too many decimal places, just return the string
		return str;
	}

	public static String toDecimalPlacesMin(float number, int minPlaces) {
		return toDecimalPlacesMinMax(number, minPlaces, Integer.MAX_VALUE);
	}

	public static String toDecimalPlacesMax(float number, int maxPlaces) {
		return toDecimalPlacesMinMax(number, 0, maxPlaces);
	}
}
