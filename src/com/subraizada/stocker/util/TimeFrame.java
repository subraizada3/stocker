/*
 *    Copyright 2018 Subramaniyam Raizada
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.subraizada.stocker.util;

public enum TimeFrame {
	// lowercase m = minute, uppercase M = month
	PERIOD_1m, PERIOD_5m, PERIOD_15m, PERIOD_30m, PERIOD_1h, PERIOD_1d, PERIOD_1w,
	TOTAL_1d, TOTAL_5d, TOTAL_1M, TOTAL_3M, TOTAL_6M, TOTAL_1y, TOTAL_2y, TOTAL_5y
}
