/*
 *    Copyright 2018 Subramaniyam Raizada
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.subraizada.stocker.util.net;

import com.subraizada.stocker.model.QuoteHistorical;
import com.subraizada.stocker.model.QuoteRealTime;
import com.subraizada.stocker.util.Debugger;
import com.subraizada.stocker.util.TimeFrame;
import com.subraizada.stocker.util.TimeUtils;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

// when markets are open, IEX Quote 'close' is previous close

public class APIIEX {
	public static JSONObject getQuoteLegacy(String ticker) throws IOException {
		String jsonStr = IOUtils.toString(new URL("https://api.iextrading.com/1.0/stock/" + ticker + "/quote"));
		return new JSONObject(jsonStr);
	}

	public static QuoteRealTime getQuote(String ticker) throws IOException {
		String jsonStr = IOUtils.toString(new URL("https://api.iextrading.com/1.0/stock/" + ticker + "/quote"));
		JSONObject json = new JSONObject(jsonStr);

		QuoteRealTime quote = new QuoteRealTime();

		// IEX API takes some time to switch from "IEX real time price" to "Close",
		// so use TimeUtils class to check whether exchanges are closed and
		// set after hours flag using that.

		String latestSource = json.getString("latestSource");
		if (latestSource.equals("IEX real time price") && !TimeUtils.isMarketOpenNow()) {
			latestSource = "Close";
		}

		// set values
		switch (latestSource) {
			case "IEX real time price":
				// market open - use IEX real time price
				quote.setAfterHours(false);
				quote.setDelayedPrice(false);
				quote.setPrice(json.getFloat("iexRealtimePrice"));
				quote.setBid(json.getFloat("iexBidPrice"));
				quote.setAsk(json.getFloat("iexAskPrice"));
				break;
			case "Close":
				quote.setAfterHours(true);
				quote.setDelayedPrice(false);
				quote.setPrice(json.getFloat("close"));
				quote.setBid(0);
				quote.setAsk(0);
				break;
			case "15 minute delayed price":
				quote.setAfterHours(false);
				quote.setDelayedPrice(true);
				quote.setPrice(json.getFloat("delayedPrice"));
				// if price is delayed, don't display IEX real time bid
				quote.setBid(0);
				quote.setAsk(0);
				break;
		}
		quote.setChangeDollars(json.getFloat("change"));
		quote.setChangePercent(json.getFloat("changePercent"));
		quote.setOpen(json.getFloat("open"));
		quote.setClose(json.getFloat("close"));
		quote.setHigh(json.getFloat("high"));
		quote.setLow(json.getFloat("low"));

		return quote;
	}

	public static HashMap<String, Float> getBatchQuote(ArrayList<String> tickers) throws IOException {
		HashMap<String, Float> result = new HashMap<>();

		String jsonStr = IOUtils.toString(new URL(
				"https://api.iextrading.com/1.0/stock/market/batch?types=quote&symbols=" +
						String.join(",", tickers)));

		JSONObject rootJsonObj = new JSONObject(jsonStr);

		for (String s : tickers) {
			JSONObject quote = rootJsonObj.getJSONObject(s).getJSONObject("quote");
			result.put(quote.getString("symbol"), quote.getFloat("latestPrice"));
		}

		return result;
	}

	public static ArrayList<QuoteHistorical> getChart(String ticker, TimeFrame timeFrame) throws IOException {
		String timeString;
		switch (timeFrame) {
			case TOTAL_1M:
				timeString = "1m";
				break;
			case TOTAL_3M:
				timeString = "3m";
				break;
			case TOTAL_6M:
				timeString = "6m";
				break;
			case TOTAL_1y:
				timeString = "1y";
				break;
			case TOTAL_2y:
			case PERIOD_1d:
				timeString = "2y";
				break;
			case TOTAL_5y:
			case PERIOD_1w:
				timeString = "5y";
				break;
			default:
				throw new IllegalArgumentException();
		}

		String urlString = "https://api.iextrading.com/1.0/stock/" + ticker + "/chart/" + timeString;
		JSONArray array = new JSONArray(IOUtils.toString(new URL(urlString)));

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

		ArrayList<QuoteHistorical> result = new ArrayList<>(array.length());

		JSONObject obj;
		for (int i = 0; i < array.length(); i++) {
			obj = array.getJSONObject(i);

			String dateString = obj.getString("date");
			LocalDateTime dateObject = LocalDate.from(dtf.parse(dateString)).atStartOfDay();

			result.add(new QuoteHistorical(new SimpleStringProperty(dateString), dateObject,
					new SimpleFloatProperty(obj.getFloat("open")),
					new SimpleFloatProperty(obj.getFloat("low")),
					new SimpleFloatProperty(obj.getFloat("high")),
					new SimpleFloatProperty(obj.getFloat("close")),
					new SimpleIntegerProperty(obj.getInt("volume"))));
		}

		Debugger.log("IEX API providing chart with " + result.size() + " data points");

		return result;
	}
}
