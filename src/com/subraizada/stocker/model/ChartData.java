/*
 *    Copyright 2018 Subramaniyam Raizada
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.subraizada.stocker.model;

import com.subraizada.stocker.util.TimeFrame;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

// TODO: make chart updater time to next update method
public class ChartData {
	private static String currentTicker = "";
	private static TimeFrame timeFrame = TimeFrame.PERIOD_1d;
	private static ObservableList<QuoteHistorical> timeSeries = FXCollections.observableArrayList();
	private static BooleanProperty outOfDate = new SimpleBooleanProperty(true);

	public static void reset() {
		currentTicker = "";
		timeSeries.clear();
		timeFrame = TimeFrame.PERIOD_1d;
		setOutOfDate(true);
	}

	public static ObservableList<QuoteHistorical> getTimeSeries() {
		return timeSeries;
	}

	public static void setTimeSeries(ObservableList<QuoteHistorical> timeSeries) {
		ChartData.timeSeries = timeSeries;
	}

	public static boolean isOutOfDate() {
		return outOfDate.get();
	}

	public static void setOutOfDate(boolean outOfDate) {
		ChartData.outOfDate.set(outOfDate);
	}

	public static BooleanProperty outOfDateProperty() {
		return outOfDate;
	}

	public static String getCurrentTicker() {
		return currentTicker;
	}

	public static void setCurrentTicker(String currentTicker) {
		ChartData.currentTicker = currentTicker;
	}

	public static TimeFrame getTimeFrame() {
		return timeFrame;
	}

	public static void setTimeFrame(TimeFrame timeFrame) {
		ChartData.timeFrame = timeFrame;
	}
}
