/*
 *    Copyright 2018 Subramaniyam Raizada
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.subraizada.stocker.model;

import com.subraizada.stocker.util.Formatting;
import com.subraizada.stocker.util.net.APICommon;
import com.subraizada.stocker.util.net.APIIEX;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javafx.application.Platform;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

public class CurrentDataUpdater {
	private static ScheduledExecutorService updateService, invalidationService;
	private static TabPane tabPane;

	public static void start(TabPane tabs) {
		tabPane = tabs;

		updateService = Executors.newScheduledThreadPool(1, r -> {
			Thread t = new Thread(r);
			t.setDaemon(true);
			return t;
		});
		updateService.scheduleAtFixedRate(CurrentDataUpdater::update, 0, 250, TimeUnit.MILLISECONDS);

		invalidationService = Executors.newScheduledThreadPool(1);
	}

	public static void stop() {
		updateService.shutdownNow();
		invalidationService.shutdownNow();
	}

	// request an update right now
	public static void refresh() {
		updateService.submit(CurrentDataUpdater::update);
	}

	// add current prices to tab labels
	private static void update() {
		// if still doing initial program loading, return
		if (!APICommon.allTickersAvailable()) return;

		// get list of opened tickers
		ArrayList<String> tickerList = new ArrayList<>();
		for (Tab tab : tabPane.getTabs()) {
			if (APICommon.isValidTicker(tab.getId())) {
				tickerList.add(tab.getId());
			}
		}

		if (tickerList.size() == 0) return;

		// get quotes from AlphaVantage API
		HashMap<String, Float> quotes;
		try {
			quotes = APIIEX.getBatchQuote(tickerList);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

		for (Map.Entry<String, Float> entry : quotes.entrySet()) {
			for (Tab tab : tabPane.getTabs()) {
				if (tab.getId().equals(entry.getKey())) {
					// this is the tab which has the price stored in this entry
					// set its label to "TCKR 20.72", ticker == entry.getKey(), price == entry.getValue()
					Platform.runLater(() ->
							tab.setText(entry.getKey() + " " + Formatting.toDecimalPlacesMinMax(entry.getValue(), 2, 3)));
				}
			}
		}

		// since update was successful, cancel any pending invalidations, and set a timer to invalidate & hide prices
		invalidationService.shutdownNow();
		invalidationService = Executors.newScheduledThreadPool(1);
		invalidationService.schedule(CurrentDataUpdater::invalidate, 2, TimeUnit.SECONDS);
	}

	// clear prices from tab labels once they are old
	private static void invalidate() {
		// if the tab's id is a ticker, set its label to just the ticker
		for (Tab tab : tabPane.getTabs())
			if (APICommon.isValidTicker(tab.getId()))
				Platform.runLater(() ->
						tab.setText(tab.getId()));
	}
}
