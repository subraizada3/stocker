/*
 *    Copyright 2018 Subramaniyam Raizada
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.subraizada.stocker.util.net;

import com.subraizada.stocker.model.QuoteHistorical;
import com.subraizada.stocker.util.Debugger;
import com.subraizada.stocker.util.TimeFrame;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

public class APIAlphaVantage {
	private static String APIKey;

	public static void setAPIKey(String APIKey) {
		APIAlphaVantage.APIKey = APIKey;
	}

	public static boolean isAPIKeyValid(String key) {
		//Preferences prefs = Preferences.userNodeForPackage(StockerApp.class);
		//String currentKey = prefs.get("AlphaVantage API Key", "");
		// AlphaVantage will accept anything for the API Key as long as it
		// is not empty or 'demo'.
		return !key.equals("") && !key.equals("demo");
	}

	public static ArrayList<QuoteHistorical> getIntradayChart(String ticker, TimeFrame timeFrame, boolean fullOutput) throws IOException {
		String timeString;
		switch (timeFrame) {
			case PERIOD_1m:
			case TOTAL_1d:
				timeString = "1min";
				break;
			case PERIOD_5m:
			case TOTAL_5d:
				timeString = "5min";
				break;
			case PERIOD_15m:
				timeString = "15min";
				break;
			case PERIOD_30m:
				timeString = "30min";
				break;
			case PERIOD_1h:
				timeString = "60min";
				break;
			default:
				throw new IllegalArgumentException();
		}

		ArrayList<QuoteHistorical> result = new ArrayList<>();

		String urlString = "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=" + ticker + "&interval=" + timeString + "&apikey=" + APIKey;
		if (fullOutput) urlString += "&outputsize=full";

		String jsonStr = IOUtils.toString(new URL(urlString));

		JSONObject rootJsonObj = new JSONObject(jsonStr);
		JSONObject chartJsonObj = rootJsonObj.getJSONObject("Time Series (" + timeString + ")");

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

		Iterator<String> keys = chartJsonObj.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			JSONObject dataObj = chartJsonObj.getJSONObject(key);

			LocalDateTime time = LocalDateTime.from(dtf.parse(key));

			QuoteHistorical quote = new QuoteHistorical(new SimpleStringProperty(key), time,
					new SimpleFloatProperty(dataObj.getFloat("1. open")),
					new SimpleFloatProperty(dataObj.getFloat("2. high")),
					new SimpleFloatProperty(dataObj.getFloat("3. low")),
					new SimpleFloatProperty(dataObj.getFloat("4. close")),
					new SimpleIntegerProperty(dataObj.getInt("5. volume")));

			result.add(quote);
		}

		Debugger.log("AV API providing chart with " + result.size() + " data points");

		return result;
	}
}
